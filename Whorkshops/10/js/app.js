function save() {
	var new_cliente = {
		nombre: document.getElementById('nombre').value,
		apellido: document.getElementById('apellido').value,
		telefono: document.getElementById('telefono').value,
	};

	if (localStorage.getItem('clientes') == null) {
		localStorage.setItem('clientes', '[]');
	}

	var load = JSON.parse(localStorage.getItem('clientes'));
	load.push(new_cliente);

	localStorage.setItem('clientes', JSON.stringify(load));

	document.getElementById('nombre').value = '';
	document.getElementById('apellido').value = '';
	document.getElementById('telefono').value = '';
}
