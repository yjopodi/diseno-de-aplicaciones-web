$(document).ready(function(){
    
    if(sessionStorage.getItem("usuario_logueado") != null){
        var user = JSON.parse(sessionStorage.getItem("usuario_logueado"));
        document.getElementById("name").innerHTML = user["nombre"];
        var producto = JSON.parse(localStorage.getItem("p_Nuevo"));
        for(var i = 0; i < producto.length; i++){
            if(producto[i]["usuario"] == user["id"]){
                var p_Base = `<div id= "producto">
                <div class="Acomodo">
                    <p id="id_Prod">${producto[i]["id"]}</>
                    <div>
                        <img src="${producto[i]["url"]}" alt="img" class="resize">
                        <div>
                            
                        </div>
                    </div>
                    <div class = "Acomodo">
                        <div>
                            <p class="title2Lp"><a href="detalle.html" onclick = "detalle(event);">${producto[i]["nombre"]}</a></p>
                            <div  class = "Acomodo">
                                <button class = "btnEditar" onclick = "editar(event);">Editar</button>
                                <div  class = "Acomodo">
                                    <button class = "btnEliminar" onclick= "eliminar(event)">Eliminar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>`;
                var produc = document.getElementById("index");
                produc.innerHTML= produc.innerHTML+p_Base;
            }

        }
    }
 });
 function detalle(event){
	const btn = event.target;
	const producto = btn.closest('#producto');
	const id = producto.querySelector('#id_Prod').textContent;
    console.log(id);
	var productos = JSON.parse(localStorage.getItem('p_Nuevo'));
	for (var i = 0; i < productos.length; i++) {
		if (productos[i]['id'] == id) {

			var pro = {
					id : productos[i]['id'],
					nombre: productos[i]['nombre'],
					url : productos[i]['url'],
					usuario : productos[i]['usuario'],
					descripcion : productos[i]['descripcion'],
					busco : productos[i]['busco'],   
				};

			localStorage.setItem('p_Select', JSON.stringify(pro));
		}
	}
} 
function eliminar(event){
	const btnEliminar = event.target;
	const producto = btnEliminar.closest('#producto');
	const id = producto.querySelector('#id_Prod').textContent;
	var productos = JSON.parse(localStorage.getItem('p_Nuevo'));
	for (var i = 0; i < productos.length; i++) {
		if (productos[i]['id'] == id){
			productos.splice(i, 1);
			localStorage.setItem('p_Nuevo', JSON.stringify(productos));
			btnEliminar.closest('#producto').remove(); 
		}
	}
}
function editar(event){
	window.location.href = 'editar.html';
	const boton = event.target;
	const producto = boton.closest('#producto');
	const id = producto.querySelector('#id_Prod').textContent;
	var productos = JSON.parse(localStorage.getItem('p_Nuevo'));
	for (var i = 0; i < productos.length; i++) {
		if (productos[i]['id'] == id) {

			var produc = {
                id : productos[i]['id'],
                nombre: productos[i]['nombre'],
                url : productos[i]['url'],
                usuario : productos[i]['usuario'],
                descripcion : productos[i]['descripcion'],
                busco : productos[i]['busco'],    
				};
			localStorage.setItem('p_Select', JSON.stringify(produc)); 
		}
	}

}