function nuevoP(){
    if(localStorage.getItem("p_Nuevo") == null){
        localStorage.setItem("p_Nuevo","[]")
    }
    var carga = JSON.parse(localStorage.getItem("p_Nuevo"));
    var cargaU = JSON.parse(sessionStorage.getItem("usuario_logueado"));
    var p_Nuevo = {id:carga.length+1,
                nombre :document.getElementById("nombre").value,
                descripcion:document.getElementById("descripcion").value,
                url:document.getElementById("url").value,
                busco:document.getElementById("busco").value,
                usuario:cargaU["id"]};
    carga.push(p_Nuevo);
    localStorage.setItem("p_Nuevo", JSON.stringify(carga));
    window.location.href = "dashboard.html";
}
