function login(){ 
	email = document.getElementById("correo").value; 
	passw = document.getElementById("contraseña").value; 

	if(email == null || email.length == 0 || email == ''){
		alert("Por favor completar todos los campos");
	  	return;
	  
	}
	else if(passw == null || passw.length == 0 || passw == ''){
		alert("Por favor completar todos los campos");
	  	return;
	}

	if (localStorage.getItem('users') == null) {
		alert("Datos Incorrectos!");
	}
	else {
		var users = JSON.parse(localStorage.getItem('users'));
		
		for (var i = users.length - 1; i >= 0; i--) {
			if (users[i]['email'] == email && users[i]['password'] == passw ) {
				document.getElementById("correo").value = '';
				document.getElementById("contraseña").value = '';

				var nuevo_usuario = {
					id : users[i]['id'], 
					email : email,
					password : passw,
					nombre : users[i]['firstN'],
				};

				sessionStorage.setItem('usuario_logueado', JSON.stringify(nuevo_usuario));
				window.location.href = 'dashboard.html';
			}
		}
	}

}