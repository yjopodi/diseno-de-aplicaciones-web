$(document).ready(function(){ 
	var select = JSON.parse(localStorage.getItem('p_Select'));
	if (select != null) {
		document.getElementById('nombre').value = select['nombre'];
		document.getElementById('descripcion').value = select['descripcion'];
		document.getElementById('url').value = select['url'];
		document.getElementById('busco').value = select['busco'];
	} 
});

function edicion(){
	var select = JSON.parse(localStorage.getItem('p_Select'));
	var productos = JSON.parse(localStorage.getItem('p_Nuevo'));
	for (var i = 0; i < productos.length; i++) {
		if(productos[i]['id'] == select['id']){

			productos[i]['nombre'] = document.getElementById('nombre').value;
			productos[i]['descripcion'] = document.getElementById('descripcion').value;
			productos[i]['url'] = document.getElementById('url').value;
			productos[i]['busco'] = document.getElementById('busco').value;	
		}
	}
	localStorage.setItem('p_Nuevo', JSON.stringify(productos));
    window.location.href='dashboard.html';
}
